#!/bin/zsh

LOG=$1

usage()
{
	cat << EOF
usage: $0:t <commitish>..<commitish>
EOF
}

if [ $# -ne 1 -o -z $LOG ]
then
	usage
	exit 27 # 27 ? Yes, 27.
fi

git-log --decorate $LOG |\
grep "\[Patch\|refs/tags" |\
sed 's/^commit ........................................ .*refs\/tags\/\([0-9]\+\)[,)].*/# Kernel \1 released here/;s/.*Patch #\([0-9]\+\)\].* Applied /Patch\1: /' |\
grep -v "^commit " |\
tac

