#!/bin/zsh

FILE=$1
TAGPREFIX=$2

usage()
{
	cat << EOF
usage: $0:t <FILE.spec> [<TAGPREFIX>]" 
	Where FILE.spec is a .spec file with list of patches and of kernel releases
	To generate this FILE.spec, you should take a look to stlinux_generate_spec_file.sh

	TAGPREFIX should be used to prefix the tags
EOF
}

if [ $# -lt 1 -o $# -gt 2 -o ! -f $FILE ]
then
	usage
	exit 42 # Yes, 42.
fi

local tag commit release

for tag in $(grep -i "^# Kernel [0-9]\+ released here$" $FILE | sed 's/^# Kernel \([0-9]*\) released here$/\1/')
do
	release=$(
		grep -i "^# Kernel $tag released here$" $FILE -B1 |\
		head -n1 |\
		sed 's/^Patch\([0-9]*\): /\\[Patch \\#\1\\].* /'
	)
	commit=$(
		git-log --grep="$release" --pretty=oneline |\
		cut -c1-40
	)
	echo "#$tag -- $(git-log --grep="$release" | grep "$release")"
	git-tag $TAGPREFIX$tag $commit
done

