#!/bin/zsh

usage()
{
	cat << EOF
usage: $0:t <STLINUX_PATCHES FOLDER> <TAG PREFIX> [Patch # start] [Patch # end]
EOF
}

if ! git rev-parse --git-dir >/dev/null
then
	echo "This is not a GIT repository !"
	usage
	exit 27 # I like 27.
fi
if [ $# -lt 2 ]
then
	usage
	exit 42 # 42 ? Because !
fi

STLINUX_FOLDER=$1
TAG_PREFIX=$2
START_FROM=${3:-0}
END_TO=${4:-}

PATCHES_LIST=$STLINUX_FOLDER/.patches

if [ ! -s $PATCHES_LIST ]
then
	:>$PATCHES_LIST
	for patch in $(grep "^%patch" $STLINUX_FOLDER/*.spec | sed 's/%\(patch[0-9]\+\).*/\1/')
	do
		print -Pn "Preparing $patch...\r"
		(
			echo $patch | sed 's/patch\([0-9]\+\)/\1/' | awk '{ printf $1" " }'
			grep -i "^\(%$patch \|$patch:\)" $STLINUX_FOLDER/*.spec | awk '{ printf $2" " }'
			echo
		) >> $PATCHES_LIST
	done
	echo

	echo "Done"
else
	echo "Patches already prepared. Skip"
fi
#cat $PATCHES_LIST

function read_patch_file()
{
	case $1:e in
		"patch")
			< $1
			;;
		"bz2")
			bunzip2 -c $1
			;;
		"*")
			echo >&2 "Error ! $1:e is not a recognized patch extension !" 
			exit;
			;;
	esac
}

export RELEASE=""
for patch in $(awk < $PATCHES_LIST '{print $1}' )
do

	file=$(grep "^$patch " $PATCHES_LIST | awk '{print $2}')
	strip=$(grep "^$patch " $PATCHES_LIST | awk '{print $3}')

	if [ $patch -ge $START_FROM -a \( -z "$END_TO" -o $patch -le "$END_TO" \) ]
	then

		if [ ! -e $STLINUX_FOLDER/$file -a -e $STLINUX_FOLDER/$file:r ] 
		then
			file=$file:r
		fi

		(
			#
			# On extrait le numéro de release ST dans le nom du fichier .patch
			# 
			_RELEASE="$(echo $file | sed 's/linux-.*[-_]stm\(.\)\(.\)[-_]\([0-9]*\)[_-\.].*/\3/')"

			# Ça a marché ? Alors on vérifie qu'on n'est pas passé à la release suivante...
			if [ "$_RELEASE" != "$file" ]
			then
				# On épure le numéro de révision
				_RELEASE=$(( $_RELEASE ))
				if [ "$(( $_RELEASE ))" -gt "$(( $RELEASE ))" ]
				then
					# Ha ! On est passé à une release "supérieure", alors on pose le tag
					git tag "$TAG_PREFIX${_RELEASE}"
					# Et on stocke le résultat pour le récupérer dans le bloc parent
					echo $_RELEASE > .git/release
				fi

				# On crée le tag pour le message de commit
				RELEASE="[$_RELEASE] "
			else
				RELEASE=""
			fi

			# Voici la première ligne du message de commit. C'est la plus jolie.
			echo "[Patch #$patch] ${RELEASE}Applied $file"

			#
			# On récupère le commentaire dans le patch en guise de message de commit
			# avec gestion des .patch ou des .patch.bz2
			# 
			read_patch_file $STLINUX_FOLDER/$file | awk '{ RS="--- "; } { if ( NR <= 2 ) { print $0 } }' | \
						grep -v -e "^diff \-" -e "^Index: " -e "^=*$" -e "^Signed-off-by: " -e "^index ";

		) | sed 's///' > .git/message ;

		# On récupère la release trouvée dans le bloc précédent
		RELEASE=$([ -s .git/release ] && ( cat .git/release ; rm -f .git/release) )
		# 
		# On affiche le résultat de l'application du patch dans le message de commit
		#
		echo "---" >> .git/message
		read_patch_file $STLINUX_FOLDER/$file | patch -N $strip >> .git/message
		# Suppression des fichiers supprimés par le patch
		git rm $(git ls-files --deleted) 2>/dev/null
		# Ajout de tout le reste :-)
		git add .
		# Et on committe !!
		git commit -F .git/message
	else

		echo >&2 "Skipping patch #$patch : $file"

	fi
done
git tag "$TAG_PREFIX$((RELEASE + 1))"
